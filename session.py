#!/usr/bin/env python
# -*- coding: utf-8 -*-   

# database component
import database

# global variable
global user_sessions
user_sessions = {}

   
def handling(request):
    
    # exit on Error
    if request['error']:
        return request

    # test if there is already a session for this user and create a new if none exists
    if request['jid'] not in user_sessions:
        user_sessions[request['jid']] = createSession(request['jid'])
        
    session = user_sessions[request['jid']]

    # actual list information
    if request['order'] == "list":
        if session.getList() == "":
                request['error'] = True
                request['error_reason'] = "No list selected!"
                return request
        request['list_name'] = session.getList()
    
    
    # set list name in session for 'create list'
    elif request['order'] == "create list":    
        # send error if the list name is already in use
        if database.getTaskList(request):
            request['error'] = True
            request['error_reason'] = "You already have a list with this name!"
        else:
            session.selectList(request['list_name'])

    
    # set list name in session for 'select list'    
    elif request['order'] == "select list":
    
        # list number is given?
        if 'list_number' in request:

            if not session.getListNameByIndex(request['list_number']):
                request['error'] = True
                request['error_reason'] = "Your number was greater than your list count OR you have to use the 'show lists' command first!"
                
            else:   
                # select list by the name taken from session object
                request['list_name'] = session.getListNameByIndex(request['list_number'])
                session.selectList(request['list_name'])
        
        elif not database.getTaskList(request):
            request['error'] = True            
            request['error_reason'] = "There is no list with this name!"
            
        else:
            session.selectList(request['list_name'])

            
    # set list name in session for 'delete list'    
    elif request['order'] == "delete list":
    
        if not database.getTaskList(request):
            request['error'] = True            
            request['error_reason'] = "There is no list with this name!"
            
            
    # these commands require a selected list
    elif request['order'] == "show list":
        
        # if no list name is given, take the one from session context
        if request['list_name'] == "":        
            # Error if no list is selected
            if session.getList() == "":
                request['error'] = True
                request['error_reason'] = "No list selected!"
                return request
                
            request['list_name'] = session.getList() 
        
    elif request['order'] == "create task":
        
        if session.getList() == "":
            request['error'] = True
            request['error_reason'] = "Please create or select a list first!"
        else:    
            request['list_name'] = session.getList()
            
    elif request['order'] == "delete task":
        
        request['list_name'] = session.getList()
        
        if session.getList() == "":
            request['error'] = True
            request['error_reason'] = "Please create or select a list first!"
            
        # task number is given?
        elif 'task_number' in request:           

            # session must contain the task ids of the last 'show list' command
            if session.getTaskIdNumber() == 0:
                request['error'] = True
                request['error_reason'] = "This list is empty or you have to use the 'show list' command first!"
            
            elif not session.getTaskIdByIndex(request['task_number']):
                request['error'] = True
                request['error_reason'] = "There are only %d tasks in your list!" % session.getTaskIdNumber()
                
            else:   
                # delete task by id taken from session object
                request['task_id'] = session.getTaskIdByIndex(request['task_number'])
                

    elif request['order'] == "set reminder":
    
        if session.getList() == "":
            request['error'] = True
            request['error_reason'] = "Please create or select a list first!"
        else:
            request['list_name'] = session.getList()
            
    return request

    
def createSession(jid, list_name=""):
    return Session(jid, list_name)

    
class Session():
    # session object for the user
    def __init__(self, jid, list_name):
        self.jid = jid
        self.list_name = list_name
        self.task_id_list = []
        self.list_names = []
        
    def selectList(self, list_name):
        self.list_name = list_name
        
    def getList(self):
        return self.list_name
        
    def setTaskIDs(self, tasks):
        self.task_id_list = []
        for task in tasks:
            self.task_id_list.append(task['id'])
    
    def getTaskIdByIndex(self, index):
        if len(self.task_id_list) >= index:
            return self.task_id_list[index-1]
        else:
            return False

    def getTaskIdNumber(self):
        return len(self.task_id_list)
        
    def getListNameByIndex(self, index):
        if len(self.list_names) >= index:
            return self.list_names[index-1]
        else:
            return False
            
    def setListNames(self, lists):
        self.list_names = []
        for list in lists:
            self.list_names.append(list[0])
            
    def getListNamesNumber(self):
        return len(self.list_names)