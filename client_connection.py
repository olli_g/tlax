#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard modules
import ssl, sys
# core component and sleekxmpp library
import core, sleekxmpp
# a string with a help text
from help import standard_help_text

#Set Encoding to UTF8
from sleekxmpp.util.misc_ops import setdefaultencoding
setdefaultencoding('utf8')


class ClientConnection(sleekxmpp.ClientXMPP):

    def __init__(self, jid, password):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        # Set session_start event
        self.add_event_handler("session_start", self.start)
        # Set message event
        self.add_event_handler("message", self.message)
        # any JID sending a request will be added to the roster of TLAX
        self.auto_subscribe = True
        self.auto_authorize = True

    def start(self, event):
        #broadcast presence
        self.send_presence()        
        #retrieve roster
        self.get_roster()

    def message(self, msg):
        # calling the message_handling function from core.py
        if msg['type'] in ('chat', 'normal'):
            core.message_handling(msg)
        
    def send_help(self, msg):
        # sends a message with the roster entries of the current JID          
        self.send_message(mto=msg['from'],mbody=standard_help_text)
        
    def send_confirmation(self, msg, text):
        # sending message to user for confirmation of his request
        self.send_message(mto=msg['from'], mbody=text)
        
    def send_notification(self, jid, listname, text):
        # sending message to user for remembering a deadline of a task
        message = "!!!!!!! Attention !!!!!!!\nThe following deadlines will end soon:\n\n"
        message += "List: [" + listname + "]\n" + text
        self.send_message(mto=jid, mbody=message)