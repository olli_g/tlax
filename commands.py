#!/usr/bin/env python
# -*- coding: utf-8 -*-

# datetime class from datetime module for the date datatype
from datetime import datetime
# function for generating a datetime object for deadline point
from manager import getDeadline
# help texts
from help import getHelpText


def parse(msg):
    # parsing the message into a dictionary
    request = {}
    # set error to True by default, set standard error reason
    request['error'] = True
    request['error_reason'] = "Could not parse command\n(send \"help\" for tips)"
    # set JID, but WITHOUT the resource part
    # stripping it from JID if there is one
    resource_index = str(msg['from']).find("/")
    if resource_index >= 0:
        request['jid'] = str(msg['from'])[:resource_index]
    else:
        request['jid'] = str(msg['from'])
    # parsing text received from user
    command = msg['body']
    # parsing user command into a list
    command_list = command.split(" ")
    # complete full command if short command is given
    command_list = getFullCommand(command_list)
    
    
    # single word commands
    if len(command_list) < 2:        
        
        request['order'] = command_list[0].lower()

        if request['order'] in ("help", "list"):
            request['error'] = False                         
        else:
            # no command dedicated, return with standard error
            return request


    # multiple word commands 
    if len(command_list) >= 2:
        
        # saving order words with space character between them
        request['order'] = command_list[0].lower() + " " + command_list[1].lower()
        
        if request['order'][:4] == "help":            
            
            request['error'] = False
            
            try:
                # try to cast the command number from the order string
                comamnd_number = int(request['order'].split(" ")[1])

                help_text = getHelpText(comamnd_number)
                
                if help_text:
                    request['help_text'] = help_text
                else:
                    request['error'] = True
                    request['error_reason'] = "Please choose a number between and 1 and 9!"
            
            except ValueError:
                request['error'] = True
                request['error_reason'] = "Command number parameter was no integer!"      
             
            # overwrite the 'order' for parsing in message_handling (core.py) 
            request['order'] = "command help"
        
        
        # 'create task' command
        elif request['order'] == "create task":
            
            request['error'] = False
            
            task_text = getContent(command)
            
            if task_text:               
                
                request['text'] = task_text
                    
                # if list of commands contains no priority, set default value to 10
                # else check if the given value is an integer
                if "p" in command_list:
                    try:
                        # find position of the priority parameter
                        pos = command_list.index("p")+1
                        # try to cast given priority into a int value
                        request['priority'] = int(command_list[pos])
                        
                    except ValueError:
                        request['error'] = True
                        request['error_reason'] = "Priority parameter was no integer!"                                             
                    except IndexError:
                        request['error'] = True
                        request['error_reason'] = "No parameter for priority!"
                        
                else:
                    request['priority'] = 10                    
                
                # if list of commands contains no deadline, set no deadline
                # else check if the given value is an integer or concrete date
                if "d" in command_list:
                    try:
                        # find position of the deadline parameter
                        pos = command_list.index("d")+1

                        if "-" in command_list[pos]:
                            # try to parse the date using the format 'YYYY-MM-DD'
                            deadline = datetime.strptime(command_list[pos],"%Y-%m-%d")
                            # save a string representation of the date (without clock time)
                            request['deadline'] = deadline.date().strftime("%Y-%m-%d --- %H:%M:%S")
                        else:
                            # try to cast given priority into a int value
                            interval = int(command_list[pos])
                            request['deadline'] = getDeadline(interval)
                            
                    except ValueError:
                        request['error'] = True
                        request['error_reason'] = "Date format was not correct!\nPlease use the format 'YYYY-MM-DD' for a precise date"\
                                                  " or only a number of days for an interval!\nFor example: '2013-12-31' or '14' "
                    except IndexError:
                        request['error'] = True
                        request['error_reason'] = "No parameter for deadline!" 

                else:
                    request['deadline'] = "---"
                    
            else:
                request['error'] = True
                request['error_reason'] = "No task name was given!"
                return request
        
        # 'delete task' command
        elif request['order'] == "delete task":
            
            request['error'] = False
            
            task_text = getContent(command)
            
            if task_text:
                request['text'] = task_text
                
            elif len(command_list) >= 3:
                request['text'] = ""
                
                try:
                    # try to cast given task number into a int value
                    request['task_number'] = int(command_list[2])
                        
                except ValueError:
                    request['error'] = True
                    request['error_reason'] = "Task number parameter was no integer!"
                    
            else:
                request['error'] = True
                request['error_reason'] = "No task name or number was given!"          
                       
            
        # commands which require a list name
        elif request['order'] == "create list":
        
            # extract the item name from the command string
            list_name = getContent(command)

            if list_name:
                request['list_name'] = list_name
                request['error'] = False
            else:
                request['error'] = True
                request['error_reason'] = "No quotation marks!"
                
                
        elif request['order'] == "select list":
        
            request['error'] = False
            
            # extract the list name from the command string
            list_name = getContent(command)
            
            if list_name:
                request['list_name'] = list_name
             
            # test if a list number was given 
            elif len(command_list) >= 3:
                request['list_name'] = ""
                
                try:
                    # try to cast given list number into a int value
                    request['list_number'] = int(command_list[2])
                        
                except ValueError:
                    request['error'] = True
                    request['error_reason'] = "List number parameter was no integer!"
                    
            else:
                request['error'] = True
                request['error_reason'] = "No task name or number was given!"
                
        
        # commands which uses session context or a name
        elif request['order'] in ("show list","delete list"):
        
            request['error'] = False
            
            # extract the item name from the command string
            list_name = getContent(command)
            
            if list_name:
                request['list_name'] = list_name
            else:
                request['list_name'] = ""
                
            
        # 'show lists' command
        elif request['order'] == "show lists":
            request['error'] = False
            
        # 'set reminder' command
        elif request['order'] == "set reminder":
            
            if len(command_list) >= 3:
                
                # extract the numbers from the command string
                values = getContent(command)
                
                if values:
                    
                    request['error'] = False
                    
                    # splitting the string at every comma
                    values = values.split(",")
                    # list to return
                    days_list = []
                    
                    for value in values:
                    
                        # cutting white spaces off
                        value = value.strip(" ")
                        
                        try:
                            # try to cast given numbers into a int value
                            days = int(value)
                            # add it to the return list, if the value is positive
                            if days >= 0:
                                days_list.append(days)
                            
                        except ValueError:
                            request['error'] = True
                            request['error_reason'] = "At least one parameter was no integer!"
                    
                    # removing duplicates from the return list by creating a set and a list again
                    # the items are also implicit sorted this way
                    request['values'] = list(set(days_list))
                    
                else:
                    request['error_reason'] = "No quotation marks!"
            
            else:
                request['error_reason'] = "No parameters!"
                    
    return request


def getContent(text):
	# test if text contains exactly 2 quotation marks
	if text.count("\"") == 2:
		# return the string between the quotation marks
		return text[text.find("\""):text.rfind("\"")+1].strip("\"")
	else:
		return False

        
def getFullCommand(command_list):
    # short versions of multiple word commands
    
    command = command_list[0].lower()
    
    if command in ("cl","sl","dl","ls","l","ct","dt","sr"):

        # save the full commands of the short versions in command_list
        if command == "cl":
            command_list.insert(1,"create")
            command_list.insert(2,"list")
        elif command == "sl":
            command_list.insert(1,"select")
            command_list.insert(2,"list")    
        elif command == "dl":
            command_list.insert(1,"delete")
            command_list.insert(2,"list")
        elif command == "ls":
            command_list.insert(1,"show")
            command_list.insert(2,"lists")
        elif command == "l":
            command_list.insert(1,"show")
            command_list.insert(2,"list")
        elif command == "ct":
            command_list.insert(1,"create")
            command_list.insert(2,"task")    
        elif command == "dt":
            command_list.insert(1,"delete")
            command_list.insert(2,"task")
        elif command == "sr":
            command_list.insert(1,"set")
            command_list.insert(2,"reminder")
            
        # remove short command
        command_list.pop(0)
            
    return command_list 