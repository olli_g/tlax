#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	This file is a part of TLAX,
	a ToDo-List Application over XMPP.
	
	author:
		Oliver Grund
		TU Chemnitz
		Germany
	
	contact:
		oliver.grund@informatik.tu-chemnitz.de		
"""

# standard modules
from argparse import ArgumentParser   
import getpass
import logging
import sys
import time

# core component
import core
    
def startTLAX():
    
    print "Starting TLAX..."
    
    # Setup the command line args.
    argParser = ArgumentParser()

    # Output verbosity options.
    argParser.add_argument('-q', '--quiet', help='set logging to ERROR', action='store_const', dest='loglevel', const=logging.ERROR, default=logging.ERROR)
    argParser.add_argument('-d', '--debug', help='set logging to DEBUG', action='store_const', dest='loglevel', const=logging.DEBUG, default=logging.ERROR)                  
    argParser.add_argument('-v', '--verbose', help='set logging to COMM', action='store_const', dest='loglevel', const=5, default=logging.ERROR)
    argParser.add_argument('-i', '--info', help='set logging to INFO', action='store_const', dest='loglevel', const=logging.INFO, default=logging.ERROR)
    
    # JID and password options.
    argParser.add_argument("-j", "--jid", dest="jid", help="JID to use")
    argParser.add_argument("-p", "--password", dest="password", help="password to use")

    # Parse the arguments
    args = argParser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel, format='%(levelname)-8s %(message)s')

    # Read login if it isn�t already set
    if args.jid is None:
        args.jid = raw_input("Jabber ID: ")
    if args.password is None:
        args.password = getpass.getpass("Password: ")
    
    # Set 'TLAX' as ressource if non is given by JID
    if len(args.jid.split("/")) == 1:
        args.jid += "/TLAX"
    
    # starting the core application component with the arguments given by the user
    core.start_core(args)

    # closing the program if 'exit' is typed in
    while(True):
        command = raw_input("")
        if command == "exit":
            print "Closing TLAX..."
            # stopping the core application component
            core.stop_core()
            # exit program
            sys.exit(0)
            
        else:
            print "Please type 'exit'"


# starts the program if the file is given directly to the python interpreter            
if __name__ == '__main__':
    startTLAX()