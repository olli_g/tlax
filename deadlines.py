#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard modules
import time, datetime, thread
# database component
import database
# string creator for tasks
from manager import FormatTaskList

# format string for date-to-string and string-to-date conversion
form = "%Y-%m-%d --- %H:%M:%S"

 
def notify(connection):
    
    # datetime object from today (but without the clock time)
    global today
    today = datetime.datetime.combine(datetime.datetime.today().date(), datetime.time())

    # iterate over all lists
    # for each list: iterate over the notification intervals
    # for each interval: iterate over all tasks and read the deadline
    # test if its time for a notification, the task is added in a task list
    # at the end of a list, the collected tasks get formatted and send to the user
    
    for list in database.getAllItems():
          
        task_list = []
        
        for task in list["todo"]:
        
            # if no deadline was specified, ignore this task
            if task["deadline"] == "---":
                continue                
        
            for interval in list["notification_days"]:            
            
                if deadline_matches_interval(interval, task["deadline"]):
                    
                    task_list.append(task)
                    continue
        
        if task_list != []:
            # send a message
            connection.send_notification(list["jid"], list['name'], FormatTaskList(task_list))
            
        
def waitForExecution(connection):
   
    # time for next clock check in seconds, 3600s = 1 hour
    sleeptime = 3600
    
    while True:
    
        current_time = datetime.datetime.now()
        
        # if its short after midnight, run notify
        # else sleep for one hour and test again       
        
        if current_time.hour == 0:
            notify(connection)
     
        time.sleep(sleeptime)

            
def deadline_matches_interval(interval, date):
    
    # datetime object with deviation of days
    distance = datetime.timedelta(interval)
    
    try:
        # generate datetime object out of the string stored in database
        dl = datetime.datetime.strptime(date,form)
        # generate new datetime object WITHOUT the clock time
        deadline = datetime.datetime(dl.year,dl.month,dl.day)
        
        # does the time deviation matches the distance in days?
        if deadline - today == distance:
            return True
        else:
            return False        
        
    except ValueError:    
        return False
        
        
def startDeadlineService(xmpp_connection):
    thread.start_new_thread(waitForExecution, (xmpp_connection,))    