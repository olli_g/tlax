#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 'numpy' is required for this script !!!
import numpy

"""
This file can interpret the files from response time recording.
You can find the RESULT.txt in the 'times' directory!

Simply change the 'file_number' variable to the number
of clients you used for recording.

Sorry for the non-existing comments in this file, but
it was more for personal use while the stresstest.

NOTE:
The standard deviation is calculated for each client individually
and the final value is a average of the standard deviation values of
the clients. This is not correct, but it will do for a normal test.
"""

file_number = 10

# minimum, maximum, average, standard deviation
overall_min = []
overall_max = []
overall_avg = []
overall_dev = []


for i in range(file_number):
    
    infile = open("times/" + str(i) + "-in.txt" , 'r')
    outfile = open("times/" + str(i) + "-out.txt" , 'r')
    
    in_times_array = []
    out_times_array = []
    
    time_in = infile.readline()
    time_out = outfile.readline()
    

    if float(time_in) - float(time_out) < 0:
        time_in = infile.readline()
        
    
    while time_in != '' and time_out != '':
        
        time_in = time_in[:-1]
        time_out = time_out[:-1]
        
        try:
            in_times_array.append(float(time_in))
            out_times_array.append(float(time_out))
            
        except ValueError:
            print "ERROR: " + str(time_in) + " & " + str(time_out)
            print infile, outfile
        
        time_in = infile.readline()
        time_out = outfile.readline()     
    
    
    result_arr = zip(in_times_array, out_times_array)
    
    final_arr = []
      
    for time in result_arr:
        
        final_arr.append(time[0] - time[1])
        
    overall_min.append(min(final_arr))
    overall_max.append(max(final_arr))
    overall_avg.append(sum(final_arr)/len(final_arr))
    
    x = numpy.array(final_arr)
    stddev = numpy.std(x)    
    overall_dev.append(stddev)
    

result = open('times/RESULT.txt', 'w')

result.write( "Min: " + str(min(overall_min)) + "\n" )
result.write( "Max: " + str(max(overall_max)) + "\n" )
result.write( "Avg: " + str(sum(overall_avg)/len(overall_avg)) + "\n" )
result.write( "Std: " + str(sum(overall_dev)/len(overall_dev)) + "\n" )

result.close()