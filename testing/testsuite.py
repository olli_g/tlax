#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard modules
import time, logging, random, string, os, sys

# importing sleekxmpp libary from parent dir (../sleekxmpp)
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import sleekxmpp

# time between sending a message in seconds
message_interval = 1

# number of lists and number of tasks per list each Client creates
list_count = 3
task_count = 3

 
class TestXMPPClient(sleekxmpp.ClientXMPP):

    def __init__(self, number, jid, password, targetJID, end_signal, recording):
    
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        # Set session_start event
        self.add_event_handler("session_start", self.start)
        # Set message event
        self.add_event_handler("message", self.message)
        # TLAX client that is tested
        self.targetJID = targetJID
        # save Bot number
        self.number = number
        # files for time measurement
        self.send_times = open("times/" + str(number) + "-out.txt", 'w')
        self.response_times = open("times/" + str(number) + "-in.txt", 'w')
        # initializing time measurement
        time.clock()
        # signal to finish client activity
        self.end_signal = end_signal
        # signal to start response time recording
        self.recording = recording
        
        
    def start(self, event):
        #broadcast presence
        self.send_presence()        
        #retrieve roster
        self.get_roster()

        
    def message(self, msg):
    
        # only record response times on signal
        if self.recording.is_set():            
            self.response_times.write(str(time.clock()) + "\n")
            

    def str_generator(self, size=8, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))

        
    def getStartOrders(self):
        
        startOrders = []
        
        for list in range(list_count):
        
            startOrders.append("create list \"List %d\"" % list)
        
            for task in range(task_count):
            
                startOrders.append("create task \"%s\" p %d d %d" % (self.str_generator(), random.randrange(20), random.randrange(30)))
                
                
        return startOrders
        
        
    def getOrders(self):
    
        # creating order sequence with random values
        return ["show lists", \
                "select list %d" % (random.randrange(list_count)), \
                "create task \"%s\" p %d d %d" % (self.str_generator(), random.randrange(20), random.randrange(30)), \
                "delete task %d" % (random.randrange(task_count)), \
                "ls", \
                "show list \"List %d\"" % (random.randrange(list_count)), \
                "list"]

      
    def start_messages(self):
        
        # initial orders (creating lists)
        for order in self.getStartOrders():
                
            self.send_message(mto=self.targetJID, mbody=order)
            if self.recording.is_set():
                self.send_times.write(str(time.clock()) + "\n")
            
            time.sleep(message_interval)
            
        
        # endless loop of read/write/delete orders
        while True:
        
            if self.end_signal.is_set():
                break
        
            for order in self.getOrders():                
                    
                self.send_message(mto=self.targetJID, mbody=order)
                
                if self.recording.is_set():
                    self.send_times.write(str(time.clock()) + "\n")
            
                time.sleep(message_interval)

            
        # stop the instance
        print "Client-Bot %d finished message sending!" % self.number
        self.disconnect(wait=True)

        

def startTestClient(number, jid, password, target, end_signal, recording):

    # Setup logging.
    logging.basicConfig(level=logging.ERROR, format='%(levelname)-8s %(message)s')
    
    t = TestXMPPClient(number, jid, password, target, end_signal, recording)
    if t.connect():
        t.process(block=False)
        time.sleep(3)
        t.start_messages()