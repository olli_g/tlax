#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard modules
import time, sys, multiprocessing
from argparse import ArgumentParser

# XMPP Client class
from testsuite import startTestClient

# TLAX instance JID for testing
target = "tlax@example.org"


if __name__ == '__main__':
    
    # Setup the command line args.
    argParser = ArgumentParser()

    argParser.add_argument('-c', '--clients', help='set the number of clients to start', type=int, default=3)

    args = argParser.parse_args()
    
    # read jid and pw for logindata.txt file in this directory
    logindata = open('logindata.txt', 'r')
    
    """
    INFORMATION:
        The content within the logindata.txt file must
        contain in each line a Jabber ID and a Password (in plaintext !!!)        
                
        The Jabebr ID and Password have to be separated by a colon,
        without any white spaces befor, after or between!
        
        For Example:
            
            jabberID@host.com:secret
            xmppaccount@aol.com:p4ssw0rd
            
        If a line starts with a '#' character or is empty it is simply skiped!

        As this is only for performance testing,
        please don´t use real existing XMPP Accounts for that purpose.
    
    IMPORTANT:
        The file MUST end with an empty line!         
    """
    
    threads = []
    end_signal = multiprocessing.Event()
    recording = multiprocessing.Event()
    
    # starting XMPP Clients in Threads
    for i in range(args.clients):

        login = logindata.readline()

        # don't go further if line is empty or starts with a '#'
        if not login or login[0] == "#" or login == "\n":
            continue
        
        # extract login data
        jid, pw = login[:-1].split(":")
      
        # starting Client in separate Thread
        p = multiprocessing.Process(target=startTestClient, args=(i,jid,pw,target,end_signal,recording,))
        p.start()
        
        threads.append(p)
        
        print "Client-Bot %d started..." % i
        
        # time gap between starting clients
        time.sleep(0.1)
       
        
    # closing the clients if 'stop' is typed in
    # recording response times if 'rec' is typed in
    while(True):    
    
        command = raw_input("")
        
        if command == "rec":
        
            # set signal to start recording the response times
            # switch off on next input
            if recording.is_set():                
                recording.clear()
                print "recording stoped..."
            else:
                recording.set()
                print "recording started..."
                
        
        elif command == "stop":
            
            # set signal to stop the clients
            end_signal.set()
                
            # exit program
            time.sleep(3)
            sys.exit(0)
            
        else:
            print "Please type 'stop' or 'rec'"
