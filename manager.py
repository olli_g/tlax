#!/usr/bin/env python
# -*- coding: utf-8 -*-

# datetime class from datetime module for the date datatype
from datetime import datetime, timedelta
# data structures for ToDo-Lists
from list import TaskList, Task
# function to generate a unique object id
from bson.objectid import ObjectId

# format string for date-to-string and string-to-date conversion
form = "%Y-%m-%d --- %H:%M:%S"

def createTask(request):
    # creating new Task object
    return Task(request['text'],request['priority'],getDate(),request['deadline'], str(ObjectId()))
    
def createList(request):
    return TaskList(request['jid'],getDate(),request['list_name'])
    
def getDate():
    # creating actual datetime object
    # converting into string using the format string
    return datetime.today().strftime(form)
    
def getDeadline(interval):
    # creating datetime object for deadline point
    return (datetime.today() + timedelta(interval)).strftime(form)
    
def FormatTaskList(task_list):
 
    if len(task_list) == 0:
        return "This list is empty!"
    
    # building string with all tasks...
    
    result = " \n##############################\n"
    result += "---------------------------------------------\n"
    for item in task_list:
        result += "ToDo: " + item['text'] + "\n"
        if item['deadline'] != "---":
            result += "Until: " + item['deadline'][:11] + "\n"
        result += "---------------------------------------------\n"
    result += "##############################"
    return result

def FormatLists(lists):    
    # sum over all tasks in all lists ('lists' variable contains tupel of name and todo array)
    task_count = sum([item[1] for item in lists])
    # building string
    result = "You have %d lists (including %d tasks) :\n" % (len(lists),task_count)
    result += "----------------------------------------\n"
    # adding all list names and task counts
    for item in lists:
        result += "[%s] (%d)\n" % (item[0], item[1])        
    return result