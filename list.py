#!/usr/bin/env python
# -*- coding: utf-8 -*-

class TaskList(dict):
    # inherit from dict (python standard datatype)
    # added attribute 'name' and 'add' function
    def __init__(self, jid, date, name="default"):
        dict.__init__(self, {})        
        self["jid"] = jid
        self["creation_date"] = date
        self["name"] = name
        self["todo"] = []
        self["notification_days"] = [1]

class Task(dict):
    # inherit from dict (python standard datatype)
    # add the given parameters to the dictionary on initialization time
    def __init__(self, text, priority, date, deadline, id):
        dict.__init__(self, {})
        self["text"] = text        
        self["priority"] = priority
        self["date"] = date
        self["deadline"] = deadline
        self["id"] = id