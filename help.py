#!/usr/bin/env python
# -*- coding: utf-8 -*-

standard_help_text = \
"""
This is TLAX, a ToDo-List Application over XMPP.

IMPORTANT: There must be a WHITESPACE between all command words,
parameter letters, parameter values and around every quoted text!

For Example:
            ct_"Go for the class reunion"_d_14_p_5
            In this command, all underscores _ must be WHITESPACES!

If you want help for a specific command, please type 'help X'!
X can be a number between 1 and 9!
Example:    help 7
                 This sends a help message for creating a task in a list

Choose which help you want:

    [1]     create a list
    [2]     show a list
    [3]     select a list
    [4]     delete a list
    [5]     show all your lists
    [6]     show which list is selected
    [7]     create a task
    [8]     delete a task
    [9]     set the days for reminding deadlines
 
"""

def getHelpText(command_number):

    if command_number == 1:
        help_text = ["##### Creating a ToDo-list #####",\
             "Full command:\n\t\t create list",\
             "Short Command:\n\t\t cl",\
             "Parameters:\n\t\t (1) a list name in quote marks",\
             "Note:\n\t\t ---",\
             "Examples:\n\t\t create list \"Shopping\" \n\t\t cl \"Family\""]
    elif command_number == 2:
        help_text = ["##### Show a ToDo-list #####",\
             "Full command:\n\t\t show list",\
             "Short Command:\n\t\t l",\
             "Parameters:\n\t\t (1) <optional> a list name in quote marks",\
             "Note:\n\t\t If there is no parameter, a selected list is required",\
             "Examples:\n\t\t show list \"Shopping\" \n\t\t l  <while a list is selected>"]
    elif command_number == 3:
        help_text = ["##### Selecting a ToDo-list #####",\
             "Full command:\n\t\t select list",\
             "Short Command:\n\t\t sl",\
             "Parameters:\n\t\t (1) a list name in quote marks",\
             "\t\t (2) a number that represents the list in the 'show lists' overview",\
             "Note:\n\t\t only one of the parameters is required",\
             "Examples:\n\t\t select list \"Shopping\" \n\t\t sl \"Family\"\n\t\t sl 3  <for selecting the third list in the list overview>"]
    elif command_number == 4:
        help_text = ["##### Deleting a ToDo-list #####",\
             "Full command:\n\t\t delete list",\
             "Short Command:\n\t\t dl",\
             "Parameters:\n\t\t (1) a list name in quote marks",\
             "Note:\n\t\t ---",\
             "Examples:\n\t\t delete list \"Shopping\" \n\t\t dl \"Family\""]
    elif command_number == 5:
        help_text = ["##### Show all ToDo-lists (Overview) #####",\
             "Full command:\n\t\t show lists",\
             "Short Command:\n\t\t ls",\
             "Parameters:\n\t\t ---",\
             "Note:\n\t\t ---",\
             "Examples:\n\t\t show lists \n\t\t sl"]
    elif command_number == 6:
        help_text = ["##### Show actual selected list #####",\
             "Full command:\n\t\t list",\
             "Short Command:\n\t\t ---",\
             "Parameters:\n\t\t ---",\
             "Note:\n\t\t shows also the reminder days for the selected list",\
             "Examples:\n\t\t list"]
    elif command_number == 7:
        help_text = ["##### Creating a ToDo-Entry #####",\
             "Full command:\n\t\t create task",\
             "Short Command:\n\t\t ct",\
             "Parameters:\n\t\t (1) a task text in quote marks",\
             "\t\t (2) <optional> a priority for the task",\
             "\t\t (3) <optional> a deadline date either in format YYYY-MM-DD",\
             "\t\t OR a number of days as integer value\n",\
             "Note:\n\t\t add the letter 'p' for a priority or 'd' for a deadline date",\
             "\t\t on your command, the order is not important",\
             "\t\t if no priority is given, it will be set to 10 by default",\
             "Examples:\n\t\t create task \"Call grandma\" \n\t\t ct \"Buy birthday present\"",\
             "\t\t create task \"Learn for final exam\" d 8  <deadline in 8 days>",\
             "\t\t ct \"Buy milk\" p 3  <priority is 3>",\
             "\t\t ct d 14 p 5 \"Go for the class reunion\""]
    elif command_number == 8:
        help_text = ["##### Deleting a ToDo-Entry #####",\
             "Full command:\n\t\t delete task",\
             "Short Command:\n\t\t dt",\
             "Parameters:\n\t\t (1) a task text in quote marks",\
             "\t\t (2) a number that represents the task in the 'show list' command",\
             "Note:\n\t\t only one of the parameters is required",\
             "\t\t if a task text is given, ALL tasks with this text will be",\
             "\t\t deleted from the selected list",\
             "Examples:\n\t\t delete task \"Call grandma\" \n\t\t dt \"Buy birthday present\"",\
             "\t\t dt 4  <to delete the fourth task in the selected list>"]
    elif command_number == 9:
        help_text = ["##### Set the days for reminding deadlines #####",\
             "Full command:\n\t\t set reminder",\
             "Short Command:\n\t\t sr",\
             "Parameters:\n\t\t (1) a list of positive integers separated with",\
             "\t\t commas and surrounded by quote marks",\
             "Note:\n\t\t a selected list is required;",\
             "\t\t white spaces in the sequence of integers are ok;",\
             "\t\t negative values are stripped without warning;",\
             "\t\t all values saved bevor are overwritten",\
             "Examples:\n\t\t set reminder \"1, 7, 14\"",\
             "\t\t sr \"2,5,10,30\""]
             
    else:
        return False

    text = ""
    for line in help_text:
        text += line + "\n"
        
    return text  