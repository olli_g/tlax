#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard modules
import time

# ClientConnection class
from client_connection import ClientConnection
# parse function
from commands import parse
# task creator
from manager import createTask, createList, FormatTaskList, FormatLists
# database component
import database
# context handling component
import session
# deadline component
from deadlines import startDeadlineService
 
 
def start_core(args):
    # start database Connection
    database.connection_setup()
    # start XMPP Connection
    # Setup the ClientConnection (as a global variable)
    global connection
    connection = connection_setup(args)
    # starting deadline remembering Service
    startDeadlineService(connection)
    
def stop_core():        
    connection.disconnect()        
    
def connection_setup(args):

    connection = ClientConnection(args.jid, args.password)
    connection.connect()
    connection.process(block=False)
    
    # wait a few seconds until the connection gets ready
    time.sleep(3)
    
    return connection
   
def message_handling(msg):
    # 1. parsing the message with parse function from commands.py
    # 2. hand the parsed message over to session handling
    # 3. store the result in the 'request' dictionary
    request = session.handling(parse(msg))    
    
    # sending an error if the command syntax was not correct
    if request['error']:
        connection.send_confirmation(msg, "[Error] %s" % request['error_reason'])        
        
    elif request['order'] == "help":
        connection.send_help(msg)
    
    elif request['order'] == "command help":
        connection.send_confirmation(msg, request['help_text'])
    
    elif request['order'] == "list":
        # reading the reminder days set for the selected list
        request['values'] = database.getTaskList(request)["notification_days"]
        connection.send_confirmation(msg, "Selected List: [%s] \nReminder days: %s" % (request['list_name'], request['values']))

    elif request['order'] == "create task":
        # send error if the list name doesn't exist
        if not database.getTaskList(request):
            connection.send_confirmation(msg, "[Error] There is no list with this name!")
            return

        database.putTaskInList(createTask(request), request)
        connection.send_confirmation(msg,"Task created! (priority %s)" % (request['priority']))
    
    elif request['order'] == "delete task":        
        
        if 'task_id' in request:            
            database.deleteTaskInListById(request)
        else:
            database.deleteTaskInList(request)
            
        connection.send_confirmation(msg,"Task deleted!")
    
    elif request['order'] == "create list":       
        database.putListInDB(createList(request))
        connection.send_confirmation(msg,"Todo list [%s] created! " % (request['list_name']))

    elif request['order'] == "show list":
        # 1. get list from database
        task_list = database.getTaskList(request)
        
        if not task_list:
            connection.send_confirmation(msg, "[Error] There is no list with this name!")
            return
        
        # 2. sort the task list by priority
        sorted_task_list = sorted(task_list['todo'], key=lambda task: task['priority'])
        
        # 3. save sorted task ids for 'delete task' command
        session.user_sessions[request['jid']].setTaskIDs(sorted_task_list)
        
        # 4. send the result with a message
        connection.send_confirmation(msg,"%s" % FormatTaskList(sorted_task_list))
        
    elif request['order'] == "delete list":
        # delete list names in user session to prevent errors with 'select list' commands
        session.user_sessions[request['jid']].setListNames([])
    
        database.deleteTaskList(request)
        connection.send_confirmation(msg,"List [%s] deleted!" % request['list_name'])        
        
    elif request['order'] == "select list":
        connection.send_confirmation(msg,"List [%s] selected!" % request['list_name'])
        
    elif request['order'] == "show lists":
        # 1. retrieving lists from database
        lists = database.getLists(request)        
        
        # 2. sort the list names alphabetical
        sorted_lists = sorted(lists, key=lambda list: list[0])
        
        # 2. save list names for 'select list' command
        session.user_sessions[request['jid']].setListNames(sorted_lists)
        
        # 3. sending response
        if len(sorted_lists) == 0:
            connection.send_confirmation(msg,"[Error] You have no list!")
        else:            
            # sending number of lists and tasks
            connection.send_confirmation(msg,FormatLists(sorted_lists))
            
    elif request['order'] == "set reminder":
        database.setReminderOfList(request)
        connection.send_confirmation(msg,"Reminder interval days for Todo list [%s] was set to %s! " % (request['list_name'], request['values']))        
        
    else:
        pass
